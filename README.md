# ML Collector backend

## Prerunning

Using virtualenv is encouraged. The project was developed with Python3.8 and is using some of
the syntax features of that version of Python.

```bash
python3.8 -m venv backend_venv
source backend_venv/bin/activate
pip install -r requirements.txt

sudo apt update
sudo apt install redis
```

>It is now assummed that virtual environment is used so every command with python is executed in that environment. If, for some reason, you do not want to use virtualenv you can simply change "python" to "python3.8"

Before running the project it is crucial to create an administrator account, like so:

```bash
cd ml_sample_collector # go to the directory with manage.py
python manage.py createsuperuser # The script will guide you through the creation process.
```

**You might want to change the interval of asynchronous task** in the ml_sample_collector/ml_sample_collector/settings.py file, at the end:

![crontab settings](readme_images/celery_beat_schedule.png)

Just change the arguments of the crontab() function. All valid arguments are listed [here](https://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html#crontab-schedules). Right now the script runs every minute, which was useful for testing purposes. In the production environment it might be better to increase the interval to at least 15 minutes.

## Running the project

```bash
cd ml_sample_collector # go to a directory with manage.py
```

Please note that following commands will "block" the terminal window so it is crucial that each command is executed either within a different terminal window or with "&" at the end so that it is moved to the background.

```bash
redis-server
celery -A ml_sample_collector worker -l INFO
python manage.py runserver
celery -A ml_sample_collector beat -l INFO
```

If everything worked fine then the server should be available at 127.0.0.1:8000.

>NOTE: If you want the server to be accessible from outside of the localhost then you may start the server with: "python manage.py runserver 0.0.0.0:8000". You may also want to change the port from 8000 to anything you want.

Now, browsing to the 127.0.0.1:8000 will result in a Page Not Found error, which is fine - after all this server is designed to primarily work with Android application. Please browse to the 127.0.0.1:8000/admin page, which should look like this:

![django admin login](readme_images/django_admin_login.png)

and log in there with the superuser account you created during "Prerunning" phase, described in the beginning of this README.

## Adding a project

In the /admin, once logged in you will be able to see the administrative section of the page, which should look like this:

![django admin](readme_images/django_admin_view.png)

If this is the first time running the project then Recent actions section will be empty.

In order to create a new project click on the "Projects" in the "COLLECTOR_BACKEND" section, you
should see something like this:

![adding project](readme_images/adding_project.png)

Please note that your list might be empty if you do not have any projects in the database. You can also easily delete projects by checking the tick near its name and then choosing "Delete selected projects" from the dropdown menu labeled as "Action".

In order to create a new project click on the gray button labeled as "ADD PROJECT +" (top right corner in the screenshot above). You will see this form:

![really adding the project now](readme_images/adding_project_but_for_real.png)

The most important part of this configuration is the Project name as this name should be unique. The project description is only for administrative purposes so that creator can write something meaningful there to somehow distinguish that project among others.

You can set "Desired photo width" and "Desired photo height" to whatever you like as those numbers will be overwritten with the values taken from the mask (labeled as "Frame" due to legacy reasons :) ). It is true that in the current state of the project we could get rid of those fields here, however potentially in the future there might be a specific behaviour defined for the situations when no frame/mask image will be provided.

The Frame itself is to be understood as a "mask". This is what will be overlayed on the photo preview in the Android application. You might want to draw something there (any shape will work). The idea here is to give user a hint where to "put" the object. This might allow to create some specific scenarios in which administrator will ask for photos with desired object present only in a specific part of a picture.

> NOTE: Frame/mask is only a hint for the user. Users can ignore it and photograph anything they want.

**Once you are happy with your project config** go ahead and click on "SAVE" button.

## Associating a Machine Learning Class with a Project

Click on "Ml classs" present on the leftside menu. You will see a very similar view to the one described in the "Adding a project" sections of this README. Go ahead and click on the "ADD ML CLASS +" button. You should see the following form:

![adding ml class](readme_images/adding_mlclass.png)

Here just write the name of the class and select the project name from the dropdown menu. This will associate that class to the project of your choosing.

> NOTE: there is a green "+" icon next to the dropdown. It is possible to create the new Project here! Clicking on that "+" will open a new window with the project creation form described in the previous section.

You may add as many classes to the project as you want.

## DISABLING DEBUG MODE

This server currently operates in the debug mode. This means that any error/exception or invalid request will make the server disclose informtion useful for debugging... and also useful for a potential attacker.

In order to set this server to a "production" mode go to the ml_sample_collector/ml_sample_collector/settings.py and look for the following section of the config (those are pretty much the first settings):

![debug settings](readme_images/settings_production.png)

As you can see from the comments:

- set DEBUG to False
- Change the SECRET_KEY (as the one in this repository is public). Additionally you may want to move it to the environment variable and fetch it via os.environ.get().
- ALLOWED_HOSTS is well... a list of allowed hosts. If you plan to use this server with a proxy you probably want to list only the IP of that proxy. "*" here means that the server accepts all connections, which is useful for debugging, but not really for production.
