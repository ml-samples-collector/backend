import os
from celery import Celery


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ml_sample_collector.settings")

celery_app = Celery("ml_sample_backend")
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()
