from django.apps import AppConfig


class CollectorBackendConfig(AppConfig):
    name = "collector_backend"
