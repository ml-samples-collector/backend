from rest_framework import serializers
from .models import MLClass, Project


class ProjectSerializer(serializers.Serializer):
    """Custom Serializer implementation used to simplify views.py code."""

    id = serializers.IntegerField(read_only=True)
    project_name = serializers.CharField(required=True, max_length=255)
    project_description = serializers.CharField(
        style={"base_template": "textarea.html"}
    )

    def create(self, validated_data):
        """Create and return a new 'Project' instance, given the validated data."""
        return Project.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """Update and return an existing 'Project' instance, given the validated data."""
        instance.project_name = validated_data.get(
            "project_name", instance.project_name
        )
        instance.project_description = validated_data.get(
            "project_description", instance.project_description
        )
        instance.save()
        return instance


class MLClassSerializer(serializers.Serializer):
    """Custom Serializer implementation used to simplify views.py code."""

    id = serializers.IntegerField(read_only=True)
    class_name = serializers.CharField(required=True, max_length=255)

    def create(self, validated_data):
        """Create and return a new 'MLClass' instance, given the validated data."""
        return MLClass.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """Update and return an existing 'MlClass' instance, given the validated data."""
        instance.class_name = validated_data.get("class_name", instance.class_name)
        instance.save()
        return instance
