from django.contrib import admin
from .models import Project, MLClass


class ProjectAdmin(admin.ModelAdmin):
    """This class is a Django way of creating an Admin page for Project model - it basically
    allows administrators to create new Projects in admin panel."""

    fields = [
        "project_name",
        "project_description",
        "desired_photo_width",
        "desired_photo_height",
        "frame",
    ]


class MlClassAdmin(admin.ModelAdmin):
    """This class is a Django way of creating an Admin page for MLClass model - it basically
    allows administrators to create new MLClasses and associate them with Projects in admin
    panel."""

    fields = ["class_name", "project"]


admin.site.register(Project, ProjectAdmin)
admin.site.register(MLClass, MlClassAdmin)
