from django.urls import path

from . import views

app_name = "collector_backend"

urlpatterns = [
    path("get_projects/", views.get_projects),
    path("get_classes/<str:project_name>", views.get_classes),
    path("get_photo_info/<str:project_name>", views.get_photo_details),
    path("get_project_frame/<str:project_name>", views.get_project_frame),
    path(
        "upload_photo/<str:project_name>/<str:mlclass>",
        views.JSONUploadPhotoView.as_view(),
    ),
    path("upload_photo", views.UploadPhotoView.as_view()),
    path("download_manager", views.DownloadManager.as_view()),
    path(
        "download/<str:project_name>/<str:mlclass_name>/<str:filename>", views.download
    ),
]
