from io import BytesIO
import os

from PIL import Image
from django.core.files.base import ContentFile
from django.db import models
from django.conf import settings


def custom_upload_to(instance, filename):
    # Neat way to create upload paths dynamically, used for Photo Model and Project Model
    if hasattr(instance, "upload_to_info"):
        path, name = instance.upload_to_info
        return f"{path}/{name}"
    else:
        # obviously this should not happen
        print("OH MY, WE HAVE SOME ISSUES")
        raise Exception


class Project(models.Model):
    """Django Model representing some kind of ML project that needs sample data to train."""

    project_name = models.CharField(max_length=255, unique=True)
    project_description = models.TextField(null=True, blank=True)
    desired_photo_width = models.IntegerField(null=True)
    desired_photo_height = models.IntegerField(null=True)
    frame = models.ImageField(null=True, blank=True, upload_to=custom_upload_to)
    no_frame_provided = models.BooleanField(null=True, default=False)

    def __str__(self):
        return self.project_name

    def clean(self):
        self._create_frame_info(self.frame)
        if not self.no_frame_provided:
            frame_upload_path_filename = os.path.join(
                settings.MEDIA_ROOT, self.project_name, "frame.png"
            )
            if os.path.exists(frame_upload_path_filename):
                os.remove(frame_upload_path_filename)

            self.set_upload_to_info(
                self.project_name, f"frame.{self.frame.name.split('.')[-1]}"
            )

    def set_upload_to_info(self, path, name):
        self.upload_to_info = (path, name)

    def _create_frame_info(self, file):
        try:
            with Image.open(file) as image:
                self.no_frame_provided = False
                # Converting image to black and white
                dimensions = image.size
                self.desired_photo_width = dimensions[0]
                self.desired_photo_height = dimensions[1]

        except ValueError:
            self.no_frame_provided = True


class MLClass(models.Model):
    """Django Model representing Machine Learning class, for example 'daisy', 'rose', 'other'
    for a Flower Recognition Project."""

    class_name = models.CharField(max_length=255)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.class_name


class Photo(models.Model):
    """Model representing an uploaded photo. It should not be possible to add those via Django
    admin page, so it is not registered in admin.py. Only way to save it is to do it once it
    was uploaded via form (programmatically)"""

    ml_class = models.ForeignKey(MLClass, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=custom_upload_to)

    def __str__(self):
        return f"{self.ml_class}|{self.image}"

    def set_upload_to_info(self, path, name):
        self.upload_to_info = (path, name)

    def save(self, *args, **kwargs):
        # before saving we need to convert image file to PNG
        if self.image:
            # filename already has a .png extension
            filename = self.image.name
            out_stream = BytesIO()
            im = Image.open(self.image).convert(mode="P", palette=Image.ADAPTIVE)
            im.save(out_stream, "png")
            # swapping old image with png converted, this also gets rid of all EXIF metadata,
            # like GPS
            self.image.save(filename, ContentFile(out_stream.getvalue()), save=False)

        super(Photo, self).save(*args, **kwargs)
