import os
import zipfile

from celery import shared_task
from django.conf import settings

from .models import Project

upload_path = settings.MEDIA_ROOT


@shared_task
def sanitize_uploaded_files():
    """Async task which will move images to zip archives. It runs periodically using crontab"""

    projects = Project.objects.all()

    for project in projects:
        path_of_interest = os.path.join(upload_path, project.project_name)
        if not os.path.exists(path_of_interest):
            # No one uploaded anything for that project
            continue

        os.chdir(path_of_interest)

        # basically all classes which have uploaded files will have a directory here
        classes_with_uploaded_files = [
            cls for cls in os.listdir() if not cls.endswith(".png")
        ]

        for mlclass in classes_with_uploaded_files:
            # Looping through every directory (MLClass) to move PNG images into zip archives
            job_path = os.path.join(path_of_interest, mlclass)
            os.chdir(job_path)
            pngs = [file for file in os.listdir() if file.endswith(".png")]
            if len(pngs) == 0:
                # no new images so we continue to the next MLClass
                continue

            zips = [file for file in os.listdir() if file.endswith(".zip")]
            if len(zips) > 0:
                recent_zip = "".join([mlclass, "_samples", str(len(zips)), ".zip"])
            else:
                # creating first archive
                recent_zip = "".join([mlclass, "_samples1.zip"])

            archive = zipfile.ZipFile(
                recent_zip, "a", zipfile.ZIP_DEFLATED, compresslevel=9
            )
            for png in pngs:
                # adding each png to an archive
                if len(archive.filelist) >= 100:
                    # we need to create a new archive if there already are 100 images in most
                    # recent one. ">=" here just to make sure if somehow there would be a zip
                    # file with more than 100 images (maybe created manually for some reason)
                    archive.close()
                    recent_zip = "".join(
                        [mlclass, "_samples", str(len(zips) + 1), ".zip"]
                    )
                    archive = zipfile.ZipFile(
                        recent_zip, "a", zipfile.ZIP_DEFLATED, compresslevel=9
                    )
                archive.write(png)
                os.remove(png)
            archive.close()
