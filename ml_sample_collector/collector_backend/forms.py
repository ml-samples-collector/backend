from django import forms


class UploadPhotoForm(forms.Form):
    """Very simply Form Class representing form which might be used by a user via browser to
    send photos."""

    file = forms.ImageField()
    data = forms.JSONField()
