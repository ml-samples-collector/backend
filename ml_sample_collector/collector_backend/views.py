import base64
import hashlib
import json
import os
import time
from io import BytesIO
from itertools import chain

from django.conf import settings
from django.core.files.images import ImageFile
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views import View

from .forms import UploadPhotoForm
from .models import Project, MLClass, Photo
from .serializers import ProjectSerializer, MLClassSerializer


def get_projects(request):
    """Queries database for all existing 'Projects', serializes them and then sends them as
    JSON"""

    projects = Project.objects.all()
    serializer = ProjectSerializer(projects, many=True)
    return JsonResponse(serializer.data, safe=False)


def get_classes(request, project_name):
    """Given the project_name it queries database for all MLClasses that are associated with
    that Project"""

    matching_classes = MLClass.objects.filter(project__project_name=project_name)
    if len(matching_classes) > 0:
        serializer = MLClassSerializer(matching_classes, many=True)
        return JsonResponse(serializer.data, safe=False)
    else:
        return HttpResponse(status=404)


def get_project_frame(request, project_name):
    """Given the project_name it queries the database for a Project and returns the frame
    (image) associated with that project."""

    project_query_set = Project.objects.filter(project_name=project_name)
    if len(project_query_set) == 0:
        return HttpResponse("PROJECT NOT FOUND", status=404)

    project = project_query_set[0]

    if project.no_frame_provided:
        return HttpResponse("NO FRAME PROVIDED", status=404)

    frame_path = os.path.join(settings.MEDIA_ROOT, project_name, "frame.png")
    try:
        with open(frame_path, "rb") as frame:
            response = HttpResponse(frame.read(), content_type="image/PNG")
            return response
    except FileNotFoundError:
        return HttpResponse("FRAME NOT FOUND, contact administrator", status=404)


def get_photo_details(request, project_name):
    """Given the project_name it queries database for a Project and returns data to help with
    camera (frame info)"""

    project_query_set = Project.objects.filter(project_name=project_name)
    if len(project_query_set) == 0:
        return HttpResponse(status=404)

    project = project_query_set[0]

    data = {
        "desired_dimensions": (
            project.desired_photo_width,
            project.desired_photo_height,
        ),
    }

    return JsonResponse(data=data)


def download(request, project_name, mlclass_name, filename):
    """View handling zip download requests."""

    file_path = os.path.abspath(
        os.path.join(settings.MEDIA_ROOT, project_name, mlclass_name, filename)
    )
    if settings.MEDIA_ROOT not in file_path:
        return HttpResponse(status=403)

    try:
        with open(file_path, "rb") as file:
            response = HttpResponse(file.read(), content_type="application/zip")
            return response
    except FileNotFoundError:
        return HttpResponse(status=404)


class DownloadManager(View):
    """View representing a Download Manager - it simply displays available download links."""

    template = "collector_backend/download_manager.html"
    download_path_prefix = "/collector/download/"

    def _build_download_path(self, project_name: str, mlclass: str) -> list:
        """Helpet method to create the download path (the URL is different than the real path
        in the system)."""

        download_path_pref = os.path.join(
            self.download_path_prefix,
            project_name,
            mlclass,
        )
        real_path = os.path.join(
            settings.MEDIA_ROOT,
            project_name,
            mlclass,
        )
        download_paths = []
        for zipfile in [
            filename for filename in os.listdir(real_path) if filename.endswith(".zip")
        ]:
            download_paths.append(os.path.join(download_path_pref, zipfile))

        return download_paths

    def get(self, request):
        """Methond handling GET requests. It creates the necessary URLs and displays them."""

        project = request.GET.get("project", "")
        mlclass = request.GET.get("mlclass", "")
        helper_data = {"project_name": None, "mlclass": None}
        context = {}

        if project != "":
            if len(Project.objects.filter(project_name=project)) == 1:
                helper_data["project_name"] = project

        if mlclass != "":
            if (
                len(
                    MLClass.objects.filter(project__project_name=project).filter(
                        class_name=mlclass
                    )
                )
                == 1
            ):
                helper_data["mlclass"] = mlclass

        if helper_data["project_name"] is not None:
            if helper_data["mlclass"] is not None:
                paths = self._build_download_path(
                    helper_data["project_name"], helper_data["mlclass"]
                )
                context["paths"] = paths
            else:
                present_classes = os.listdir(
                    os.path.join(settings.MEDIA_ROOT, helper_data["project_name"])
                )
                paths = []
                for cls in present_classes:
                    if not cls.endswith(".png"):
                        paths.append(
                            self._build_download_path(helper_data["project_name"], cls)
                        )

                paths = list(chain.from_iterable(paths))
                context["paths"] = paths
        else:
            context["paths"] = ["BAD PROJECT NAME"]

        return render(request=request, template_name=self.template, context=context)


class JSONUploadPhotoView(View):
    """View handling Photo upload via JSON POST, this method is used by Android."""

    base_upload_dir = settings.MEDIA_ROOT

    def post(self, request, *args, **kwargs):
        """Method handling POST requests. It simply gets the base64 encoded image, converts it
        to Image file and then allows the server to process it and store in a proper place in
        the filesystem."""

        project = kwargs["project_name"]
        mlclass = kwargs["mlclass"]
        json_data = json.loads(request.body.decode("utf-8"))
        file = json_data["file"]
        file_decoded = base64.b64decode(file)

        buffer = BytesIO()
        buffer.write(file_decoded)
        buffer.seek(0)

        uploaded_photo = ImageFile(buffer, name=f"temp_{mlclass}.png")
        self.handle_uploaded_image(uploaded_photo, project, mlclass)
        return HttpResponse(200)

    def handle_uploaded_image(self, image, project, mlclass_name):
        # django will now that relative_path is in regards to MEDIA_ROOT
        relative_path = os.path.join(project, mlclass_name)
        if (
            len(
                mlclass := MLClass.objects.filter(project__project_name=project).filter(
                    class_name=mlclass_name
                )
            )
            == 1
        ):
            # that should be always the case
            mlclass = mlclass[0]
        else:
            raise Exception("ERROR: FILTERING RETURNED MORE THAN ONE CLASS")

        new_photo = Photo(ml_class=mlclass)

        # each image will have a filename of "<MLClass>_<8_characters_of_random>.png"
        # random part will be generator by calculating hash of current time and taking first
        # 8 characters
        generator = hashlib.sha256(bytes(str(time.time()), "utf-8"))
        random_part = generator.hexdigest()[:8]
        name = "".join([mlclass_name, "_", random_part, ".png"])

        new_photo.set_upload_to_info(relative_path, name)
        new_photo.image = image
        new_photo.save()


class UploadPhotoView(View):
    """View handling Photo upload via HTML form. It might be used by users via browser. However
    the primary way of uploading files is via JSON (see JSONUploadPhotoView)."""

    template = "collector_backend/data_upload.html"
    base_upload_dir = settings.MEDIA_ROOT

    def get(self, request):
        """Handling GET requests, it simply creates a form and displays it to the user."""

        form = UploadPhotoForm()
        return render(request, self.template, {"form": form})

    def post(self, request):
        """Handling POST requests. It retrieves the data from the form and handles data."""

        form = UploadPhotoForm(request.POST, request.FILES)
        if form.is_valid():
            self.handle_uploaded_image(
                form.cleaned_data["file"],
                json.loads(form.cleaned_data["data"].replace("'", '"')),
            )
            return HttpResponse(status=204)
        else:
            return HttpResponse(status=400)

    def handle_uploaded_image(self, image, json_data):
        project = json_data["project"]
        mlclass = json_data["mlclass"]

        self.save_image(image, project, mlclass)

    def save_image(self, image, project, mlclass_name):
        # django will now that relative_path is in regards to MEDIA_ROOT
        relative_path = os.path.join(project, mlclass_name)
        if (
            len(
                mlclass := MLClass.objects.filter(project__project_name=project).filter(
                    class_name=mlclass_name
                )
            )
            == 1
        ):
            # that should be always the case
            mlclass = mlclass[0]
        else:
            raise Exception("ERROR: FILTERING RETURNED MORE THAN ONE CLASS")

        new_photo = Photo(ml_class=mlclass)

        # each image will have a filename of "<MLClass>_<8_characters_of_random>.png"
        # random part will be generator by calculating hash of current time and taking first
        # 8 characters
        generator = hashlib.sha256(bytes(str(time.time()), "utf-8"))
        random_part = generator.hexdigest()[:8]
        name = "".join([mlclass_name, "_", random_part, ".png"])
        new_photo.set_upload_to_info(relative_path, name)
        new_photo.image = image
        new_photo.save()
